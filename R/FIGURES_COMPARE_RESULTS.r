#########################################################################################################################
## log axes for plotting
all.ss$logE_rain_median      = log(all.ss$rain_median)
all.ss$logE_rain_span        = log(all.ss$rain_span)
trees.ss$logE_rain_median    = log(trees.ss$rain_median)
trees.ss$logE_rain_span      = log(trees.ss$rain_span)

all.ss$logE_nitrogen_median   = log(all.ss$nitrogen_median)
all.ss$logE_nitrogen_span     = log(all.ss$nitrogen_span)
trees.ss$logE_nitrogen_median = log(trees.ss$nitrogen_median)
trees.ss$logE_nitrogen_span   = log(trees.ss$nitrogen_span)


all$logE_rain_median       = log(all$rain_median)
all$logE_rain_span         = log(all$rain_span)
trees$logE_rain_median     = log(trees$rain_median)
trees$logE_rain_span       = log(trees$rain_span)

all$logE_nitrogen_median   = log(all$nitrogen_median)
all$logE_nitrogen_span     = log(all$nitrogen_span)
trees$logE_nitrogen_median = log(trees$nitrogen_median)
trees$logE_nitrogen_span   = log(trees$nitrogen_span)


#########################################################################################################################
## run the phylo-corrected linear regressions
phy.mod.rain.all        <- phylolm(logE_rain_span     ~ logE_rain_median,     data = all.ss,  phy = burley_zanne)
phy.mod.temp.all        <- phylolm(temp_span          ~ temp_median,          data = all.ss,  phy = burley_zanne)
phy.mod.nitrogen.all    <- phylolm(logE_nitrogen_span ~ logE_nitrogen_median, data = all.ss,  phy = burley_zanne)

phy.mod.rain.trees      <- phylolm(logE_rain_span     ~ logE_rain_median,     data = trees.ss,  phy = burley_zanne_tree)
phy.mod.temp.trees      <- phylolm(temp_span          ~ temp_median,          data = trees.ss,  phy = burley_zanne_tree)
phy.mod.nitrogen.trees  <- phylolm(logE_nitrogen_span ~ logE_nitrogen_median, data = trees.ss,  phy = burley_zanne_tree)


## run standard lms
non.phy.mod.rain.all      <- lm(logE_rain_span ~ logE_rain_median,     data = all.ss)
non.phy.mod.temp.all      <- lm(temp_span      ~ temp_median,          data = all.ss)
non.phy.mod.nitrogen.all  <- lm(logE_rain_span ~ logE_nitrogen_median, data = all.ss)

non.phy.mod.rain.tree     <- lm(logE_rain_span ~ logE_rain_median,     data = trees.ss)
non.phy.mod.temp.tree     <- lm(temp_span      ~ temp_median,          data = trees.ss)
non.phy.mod.nitrogen.tree <- lm(logE_rain_span ~ logE_nitrogen_median, data = trees.ss)


## the run GAMs for rain and nitrogen, as per main analysis, 
## but this time just for the subset of species on the phylogeny


## run gams and lms on manuscript species
log.rain.gam.all        = gam(log(rain_span) ~ s(log(rain_median), k = 3),         data = all)
log.rain.gam.trees      = gam(log(rain_span) ~ s(log(rain_median), k = 3),         data = trees)

log.nitrogen.gam.all    = gam(log(nitrogen_span) ~ s(log(nitrogen_median), k = 3), data = all)
log.nitrogen.gam.trees  = gam(log(nitrogen_span) ~ s(log(nitrogen_median), k = 3), data = trees)

temp.lm.all    = lm(temp_span ~ temp_median, data = all)
temp.lm.trees  = lm(temp_span ~ temp_median, data = trees)




##########################################################################################################################
## Now plot another two versions of FIG 3 from the manuscript, but this 
## time we are comparing the species results with the phylolms.


##########################################################
## New FIG 3A, all species
par(mfrow = c(3, 2),
    oma   = c(1.5, 1.5, 1.5, 1.5))

##
par(font.lab = 2)
plot(log(all$rain_median), log(all$rain_span),              
     pch  = 19, col = "blue", las = 1, cex = 0.8,
     xlab = "Species log (RP)", 
     ylab = "Species log (RW)")

lines(sort(all$logE_rain_median), fitted(log.rain.gam.all)
      [order(all$logE_rain_median)], col= 'orange', type = 'l', lwd = 3)

legend("bottomright", bty = "n", cex = 1.1, pt.cex = 3, 
       text.col = "black", legend = paste("DE =", format(summary(log.rain.gam.all)$dev.expl, digits = 3)))

##
par(font.lab = 2)
plot(log(all.ss$rain_median), log(all.ss$rain_span),              
     pch  = 19, col = "blue", las = 1, cex = 0.8,
     xlab = "Phylo spp log (RP)", 
     ylab = "Phylo spp log (RW)")

abline(phy.mod.rain.all, col = "orange", lwd = 3)

# legend("topright", bty = "n", cex = 4, pt.cex = 1.1, text.col = "black", 
#        legend = paste("Adj Rsq =", format(summary(phy.mod.rain.all)$adj.r.squared, digits = 2)))	 


##
par(font.lab = 2)
plot(all$temp_median, all$temp_span,                        
     pch  = 19,  col = "orange", las = 1, cex = 0.8, 
     xlab = "Species TP (°C)", 
     ylab = "Species TW (°C)")

abline(temp.lm.all, col = "black", lwd = 3)

legend("topright", bty = "n", cex = 1.1, pt.cex = 4, text.col = "black", 
       legend = paste("Adj Rsq =", format(summary(temp.lm.all)$adj.r.squared, digits = 2)))	 


##
par(font.lab = 2)
plot(all.ss$temp_median, all.ss$temp_span,                        
     pch  = 19,  col = "orange", las = 1, cex = 0.8, 
     xlab = "Phylo spp TP (°C)", 
     ylab = "Phylo spp TW (°C)")

abline(phy.mod.temp.all, col = "black", lwd = 3)


##
par(font.lab = 2)
plot(log(all$nitrogen_median), log(all$nitrogen_span),      
     pch  = 19, col = "purple", las = 1, cex = 0.8, 
     xlab = "Species log (NP)", 
     ylab = "Species log (NW)")

lines(sort(all$logE_nitrogen_median), fitted(log.nitrogen.gam.all)
      [order(all$logE_nitrogen_median)], col= 'orange', type = 'l', lwd = 3)

legend("bottomright", bty = "n", cex = 1.1, pt.cex = 3, 
       text.col = "black", legend = paste("DE =", format(summary(log.nitrogen.gam.all)$dev.expl, digits = 3)))

## 
par(font.lab = 2)
plot(log(all.ss$nitrogen_median), log(all.ss$nitrogen_span),      
     pch  = 19, col = "purple", las = 1, cex = 0.8,
     xlab = "Phylo spp log (NP)", 
     ylab = "Phylo spp log (NW)")

abline(phy.mod.nitrogen.all, col = "orange", lwd = 3)



##########################################################
## New FIG 3B, tree species
par(mfrow = c(3, 2),
    oma   = c(1.5, 1.5, 1.5, 1.5))

##
par(font.lab = 2)
plot(log(trees$rain_median), log(trees$rain_span),              
     pch  = 19, col = "blue", las = 1, cex = 0.8,
     xlab = "Species log (RP)", 
     ylab = "Species log (RW)")

lines(sort(trees$logE_rain_median), fitted(log.rain.gam.trees)
      [order(trees$logE_rain_median)], col= 'orange', type = 'l', lwd = 3)

legend("bottomright", bty = "n", cex = 1.1, pt.cex = 3, 
       text.col = "black", legend = paste("DE =", format(summary(log.rain.gam.trees)$dev.expl, digits = 3)))

##
par(font.lab = 2)
plot(log(trees.ss$rain_median), log(trees.ss$rain_span),              
     pch  = 19, col = "blue", las = 1, cex = 0.8,
     xlab = "Phylo spp log (RP)", 
     ylab = "Phylo spp log (RW)")

abline(phy.mod.rain.trees, col = "orange", lwd = 3)

# legend("topright", bty = "n", cex = 4, pt.cex = 1.1, text.col = "black", 
#        legend = paste("Adj Rsq =", format(summary(phy.mod.rain.trees)$adj.r.squared, digits = 2)))	 


##
par(font.lab = 2)
plot(trees$temp_median, trees$temp_span,                        
     pch  = 19,  col = "orange", las = 1, cex = 0.8, 
     xlab = "Species TP (°C)", 
     ylab = "Species TW (°C)")

abline(temp.lm.trees, col = "black", lwd = 3)

legend("topright", bty = "n", cex = 1.1, pt.cex = 4, text.col = "black", 
       legend = paste("Adj Rsq =", format(summary(temp.lm.trees)$adj.r.squared, digits = 2)))	 


##
par(font.lab = 2)
plot(trees.ss$temp_median, trees.ss$temp_span,                        
     pch  = 19,  col = "orange", las = 1, cex = 0.8, 
     xlab = "Phylo spp TP (°C)", 
     ylab = "Phylo spp TW (°C)")

abline(phy.mod.temp.trees, col = "black", lwd = 3)


##
par(font.lab = 2)
plot(log(trees$nitrogen_median), log(trees$nitrogen_span),      
     pch  = 19, col = "purple", las = 1, cex = 0.8, 
     xlab = "Species log (NP)", 
     ylab = "Species log (NW)")

lines(sort(trees$logE_nitrogen_median), fitted(log.nitrogen.gam.trees)
      [order(trees$logE_nitrogen_median)], col= 'orange', type = 'l', lwd = 3)

legend("bottomright", bty = "n", cex = 1.1, pt.cex = 3, 
       text.col = "black", legend = paste("DE =", format(summary(log.nitrogen.gam.trees)$dev.expl, digits = 3)))

## 
par(font.lab = 2)
plot(log(trees.ss$nitrogen_median), log(trees.ss$nitrogen_span),      
     pch  = 19, col = "purple", las = 1, cex = 0.8,
     xlab = "Phylo spp log (NP)", 
     ylab = "Phylo spp log (NW)")

abline(phy.mod.nitrogen.trees, col = "orange", lwd = 3)




##########################################################################################################################
## residuals for phylm rain ~ width position: create panel for these...
## can use for loops, etc to abbreviate this...
par(mfrow = c(3, 2),
    oma   = c(1.5, 1.5, 1.5, 1.5))

## residuals for gam of rain ~ width position
par(font.lab = 2)

plot(fitted(log.rain.gam.all), resid(log.rain.gam.all),              
     pch  = 19, col = "blue", las = 1, cex = 0.5,
     xlab = "fitted gam(log(RW) ~ log(RP))", 
     ylab = "residuals")

abline(h = 0, col = "orange", lwd = 3)

## residuals for phylm of rain ~ width position
par(font.lab = 2)
plot(fitted(phy.mod.rain.all), resid(phy.mod.rain.all),              
     pch  = 19, col = "blue", las = 1, cex = 0.5,
     xlab = "fitted phylm(log(RW) ~ log(RP))", 
     ylab = "")

abline(h = 0, col = "orange", lwd = 3)


## residuals for lm temp ~ width position: create panel for these...
par(font.lab = 2)

plot(fitted(temp.lm.all), resid(temp.lm.all),              
     pch  = 19, col = "orange", las = 1, cex = 0.5,
     xlab = "fitted lm(TW ~ TP)", 
     ylab = "residuals")

abline(h = 0, col = "black", lwd = 3)


## residuals for phylm temp ~ width position: create panel for these...
par(font.lab = 2)

plot(fitted(phy.mod.temp.all), resid(phy.mod.temp.all),              
     pch  = 19, col = "orange", las = 1, cex = 0.5,
     xlab = "fitted phylm(TW ~ TP)", 
     ylab = "")

abline(h = 0, col = "black", lwd = 3)


## residuals for gam of nitrogen ~ width position
par(font.lab = 2)

plot(fitted(log.nitrogen.gam.all), resid(log.nitrogen.gam.all),              
     pch  = 19, col = "purple", las = 1, cex = 0.5,
     xlab = "fitted gam(log(NW) ~ log(NP))", 
     ylab = "residuals")

abline(h = 0, col = "orange", lwd = 3)


## residuals for phylm nitrogen ~ width position: create panel for these...
par(font.lab = 2)

plot(fitted(phy.mod.nitrogen.all), resid(phy.mod.nitrogen.all),              
     pch  = 19, col = "purple", las = 1, cex = 0.5,
     xlab = "fitted phylm(log(NW) ~ log(NP))", 
     ylab = "")

abline(h = 0, col = "orange", lwd = 3)

