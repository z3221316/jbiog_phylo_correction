## take log base E of species niches for plotting
all.spp    = all
trees.spp  = trees ## dim(trees.spp)
all.spp$logE_rain_median       = log(all.spp$rain_median)
all.spp$logE_rain_span         = log(all.spp$rain_span)
trees.spp$logE_rain_median     = log(trees.spp$rain_median)
trees.spp$logE_rain_span       = log(trees.spp$rain_span)

all.spp$logE_nitrogen_median   = log(all.spp$nitrogen_median)
all.spp$logE_nitrogen_span     = log(all.spp$nitrogen_span)
trees.spp$logE_nitrogen_median = log(trees.spp$nitrogen_median)
trees.spp$logE_nitrogen_span   = log(trees.spp$nitrogen_span)


## take log base E of genus niches for plotting
all.genus$logE_rain_median       = log(all.genus$rain_median)
all.genus$logE_rain_span         = log(all.genus$rain_span)
trees.genus$logE_rain_median     = log(trees.genus$rain_median)
trees.genus$logE_rain_span       = log(trees.genus$rain_span)

all.genus$logE_nitrogen_median   = log(all.genus$nitrogen_median)
all.genus$logE_nitrogen_span     = log(all.genus$nitrogen_span)
trees.genus$logE_nitrogen_median = log(trees.genus$nitrogen_median)
trees.genus$logE_nitrogen_span   = log(trees.genus$nitrogen_span)


################################################################################################################
## RUN GENERALISED ADDITIVE MODELS FOR RAIN AND NITROGEN USING SPECIES AND GENERA 
log.rain.gam.all.spp         = gam(log(rain_span) ~ s(log(rain_median), k = 3), data = all.spp)
log.rain.gam.trees.spp       = gam(log(rain_span) ~ s(log(rain_median), k = 3), data = trees.spp)

log.rain.gam.all.genus       = gam(log(rain_span) ~ s(log(rain_median), k = 3), data = all.genus)
log.rain.gam.trees.genus     = gam(log(rain_span) ~ s(log(rain_median), k = 3), data = trees.genus)

log.nitrogen.gam.all.spp     = gam(log(nitrogen_span) ~ s(log(nitrogen_median), k = 3), data = all.spp)
log.nitrogen.gam.trees.spp   = gam(log(nitrogen_span) ~ s(log(nitrogen_median), k = 3), data = trees.spp)

log.nitrogen.gam.all.genus   = gam(log(nitrogen_span) ~ s(log(nitrogen_median), k = 3), data = all.genus)
log.nitrogen.gam.trees.genus = gam(log(nitrogen_span) ~ s(log(nitrogen_median), k = 3), data = trees.genus)


################################################################################################################
## LINEAR MODELS FOR TEMP 
temp.lm.all.spp      = lm(temp_span ~ temp_median, data = all.spp)
temp.lm.trees.spp    = lm(temp_span ~ temp_median, data = trees.spp)

temp.lm.all.genus    = lm(temp_span ~ temp_median, data = all.genus)
temp.lm.trees.genus  = lm(temp_span ~ temp_median, data = trees.genus)


############################################################################################################
## PLOT GENUS RELATIONSHIPS


############################################################################################################
## GAM SPECIES log(RAIN) vs GAM GENUS log(RAIN)
par(mfrow = c(1, 2),
    oma   = c(1.5, 1.5, 1.5, 1.5))


## SPECIES
par(font.lab = 2)
plot(all.spp$logE_rain_median, all.spp$logE_rain_span, pch = 19, col = "blue",
     las = 1, cex = 0.5,
     xlab = "Species log (RP)", 
     ylab = "Species log (RW)")

lines(sort(all.spp$logE_rain_median), fitted(log.rain.gam.all.spp)
      [order(all.spp$logE_rain_median)], col = 'orange', type = 'l', lwd = 4)


## GENERA
par(font.lab = 2)
plot(all.genus$logE_rain_median, all.genus$logE_rain_span, pch = 19, col = "blue",
     las = 1, cex = 0.5,
     xlab = "Genera log (RP)", 
     ylab = "Genera log (RW)")

lines(sort(all.genus$logE_rain_median), fitted(log.rain.gam.all.genus)
      [order(all.genus$logE_rain_median)], col = 'orange', type = 'l', lwd = 4)


############################################################################################################
## GAM SPECIES log(RAIN) vs GAM GENUS log(RAIN)
par(mfrow = c(1, 2),
    oma   = c(1.5, 1.5, 1.5, 1.5))
## GAM TREE SPECIES log(RAIN) vs GAM TREE GENUS log(RAIN)
## TREE SPECIES
par(font.lab = 2)
plot(trees.spp$logE_rain_median, trees.spp$logE_rain_span, pch = 19, col = "blue",
     las = 1, cex = 0.5,
     xlab = "Tree species log (RP)", 
     ylab = "Tree species log (RW)")

lines(sort(trees.spp$logE_rain_median), fitted(log.rain.gam.trees.spp)
      [order(trees.spp$logE_rain_median)], col = 'orange', type = 'l', lwd = 4)


## TREE GENERA
par(font.lab = 2)
plot(trees.genus$logE_rain_median, trees.genus$logE_rain_span, pch = 19, col = "blue",
     las = 1, cex = 0.5,
     xlab = "Tree genera log (RP)", 
     ylab = "Tree genera (RW)")

lines(sort(trees.genus$logE_rain_median), fitted(log.rain.gam.trees.genus)
      [order(trees.genus$logE_rain_median)], col = 'orange', type = 'l', lwd = 4)





############################################################################################################
## GAM ALL SPECIES log(temp) vs GAM ALL GENUS log(temp)
par(mfrow = c(1, 2),
    oma   = c(1.5, 1.5, 1.5, 1.5))

## SPECIES
par(font.lab = 2)
plot(all.spp$temp_median, all.spp$temp_span, pch = 19, col = "orange",
     las = 1, cex = 0.5,
     xlab = "Species log (TP)", 
     ylab = "Species log (TW)")

lines(sort(all.spp$temp_median), fitted(temp.lm.all.spp)
      [order(all.spp$temp_median)], col = 'black', type = 'l', lwd = 4)


## GENERA
par(font.lab = 2)
plot(all.genus$temp_median, all.genus$temp_span, pch = 19, col = "orange",
     las = 1, cex = 0.5,
     xlab = "Genera TP", 
     ylab = "Genera TW")

lines(sort(all.genus$temp_median), fitted(temp.lm.all.genus)
      [order(all.genus$temp_median)], col = 'black', type = 'l', lwd = 4)


## GAM ALL SPECIES log(temp) vs GAM ALL GENUS log(temp)
par(mfrow = c(1, 2),
    oma   = c(1.5, 1.5, 1.5, 1.5))

## GAM TREE SPECIES log(temp) vs GAM TREE GENUS log(temp)
## SPECIES
par(font.lab = 2)
plot(trees.spp$temp_median, trees.spp$temp_span, pch = 19, col = "orange",
     las = 1, cex = 0.5,
     xlab = "Tree species TP", 
     ylab = "Tree species log TW")

lines(sort(trees.spp$temp_median), fitted(temp.lm.trees.spp)
      [order(trees.spp$temp_median)], col = 'black', type = 'l', lwd = 4)


## GENERA
par(font.lab = 2)
plot(trees.genus$temp_median, trees.genus$temp_span, pch = 19, col = "orange",
     las = 1, cex = 0.5,
     xlab = "Tree genera TP", 
     ylab = "Tree genera TW")

lines(sort(trees.genus$temp_median), fitted(temp.lm.trees.genus)
      [order(trees.genus$temp_median)], col = 'black', type = 'l', lwd = 4)





############################################################################################################
## GAM SPECIES log(RAIN) vs GAM GENUS log(RAIN)
par(mfrow = c(1, 2),
    oma   = c(1.5, 1.5, 1.5, 1.5))

## SPECIES
par(font.lab = 2)
plot(all.spp$logE_nitrogen_median, all.spp$logE_nitrogen_span, pch = 19, col = "purple",
     las = 1, cex = 0.5,
     xlab = "Species log (NP)", 
     ylab = "Species log (NW)")

lines(sort(all.spp$logE_nitrogen_median), fitted(log.nitrogen.gam.all.spp)
      [order(all.spp$logE_nitrogen_median)], col = 'orange', type = 'l', lwd = 4)

## GENERA
par(font.lab = 2)
plot(all.genus$logE_nitrogen_median, all.genus$logE_nitrogen_span, pch = 19, col = "purple",
     las = 1, cex = 0.5,
     xlab = "Genera log (NP)", 
     ylab = "Genera log (NW)")

lines(sort(all.genus$logE_nitrogen_median), fitted(log.nitrogen.gam.all.genus)
      [order(all.genus$logE_nitrogen_median)], col = 'orange', type = 'l', lwd = 4)


## GAM TREE SPECIES log(NAIN) vs GAM TREE GENUS log(NAIN)
## TREE SPECIES
par(mfrow = c(1, 2),
    oma   = c(1.5, 1.5, 1.5, 1.5))

par(font.lab = 2)
plot(trees.spp$logE_nitrogen_median, trees.spp$logE_nitrogen_span, pch = 19, col = "purple",
     las = 1, cex = 0.5,
     xlab = "Tree species log (NP)", 
     ylab = "Tree species log (NW)")

lines(sort(trees.spp$logE_nitrogen_median), fitted(log.nitrogen.gam.trees.spp)
      [order(trees.spp$logE_nitrogen_median)], col = 'orange', type = 'l', lwd = 4)


## TREE GENERA
par(font.lab = 2)
plot(trees.genus$logE_nitrogen_median, trees.genus$logE_nitrogen_span, pch = 19, col = "purple",
     las = 1, cex = 0.5,
     xlab = "Tree genera log (NP)", 
     ylab = "Tree genera (NW)")

lines(sort(trees.genus$logE_nitrogen_median), fitted(log.nitrogen.gam.trees.genus)
      [order(trees.genus$logE_nitrogen_median)], col = 'orange', type = 'l', lwd = 4)

