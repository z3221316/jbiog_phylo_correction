table.columns = c(
    "Model",
    "MS_N",
    "Phy_N",
    "Parameter",
    "MS_result",
    "Phy_result",
    "Lm_result")     


model_list_H1   = c("Rainfall log(NW) ~ log(NP)",  
                    "Rainfall log(NW) ~ log(NP)",      
                    "Rainfall log(NW) ~ log(NP)",   
                    "Rainfall log(NW) ~ log(NP)",  
                    
                    "temp NW ~ NP",  
                    "temp NW ~ NP",
                    "temp NW ~ NP",  
                    "temp NW ~ NP",
                    
                    "Nitrogen log(NW) ~ log(NP)",  
                    "Nitrogen log(NW) ~ log(NP)",      
                    "Nitrogen log(NW) ~ log(NP)",   
                    "Nitrogen log(NW) ~ log(NP)")


## could change this a bit to incorporate the GAMs too
parameter_list  = c("intercept",  
                    "coefficient",   
                    "test-statistic", 
                    "p-value", 
                    
                    "intercept",  
                    "coefficient",   
                    "test-statistic", 
                    "p-value",
                    
                    "intercept",  
                    "coefficient",   
                    "test-statistic", 
                    "p-value")
  

## build the columns and rows to store the results
table.length                    = length(table.columns)
RESULTS                         = matrix(0, 12, table.length)
colnames(RESULTS)               = table.columns 
H1.COMPARE.RESULTS              = as.data.frame(RESULTS)
H1.COMPARE.RESULTS$Phy_N        = length(all.ss$Genus)
H1.COMPARE.RESULTS$MS_N         = length(all$Genus)
H1.COMPARE.RESULTS$Model        = model_list_H1
H1.COMPARE.RESULTS$Parameter    = parameter_list
H1.COMPARE.RESULTS


## now fill this table with values for the phylo, lm and gam regressions....
## should we include the manuscript p-values, to emphasise the point that
## phylolm p-values are just as unreliable, perhaps spatial autocorrelation
## has more potential to influence results than phylogenetic autocorrelation...?


## using functions: these create numeric lists, so the tables can be stitched back together
comp.phy.mod.rain.all         = comp_phylolm_log_width_log_position(df = all.ss, colname = "rain",     phy = burley_zanne)
comp.phy.mod.temp.all         = comp_phylolm_width_position(df = all.ss,         colname = "temp",     phy = burley_zanne)
comp.phy.mod.nitrogen.all     = comp_phylolm_log_width_log_position(df = all.ss, colname = "nitrogen", phy = burley_zanne)

comp.non.phy.mod.rain.all     = comp_lm_log_width_log_position(df = all.ss,      colname = "rain")
comp.non.phy.mod.temp.all     = comp_lm_width_position(df = all.ss,              colname = "temp")
comp.non.phy.mod.nitrogen.all = comp_lm_log_width_log_position(df = all.ss,      colname = "nitrogen")

H1.phylolm = c(comp.phy.mod.rain.all, comp.phy.mod.temp.all, comp.phy.mod.nitrogen.all)
H1.lm      = c(comp.non.phy.mod.rain.all, comp.non.phy.mod.temp.all, comp.non.phy.mod.nitrogen.all)


## now stitch results in:
H1.COMPARE.RESULTS$Phy_result     = H1.phylolm
H1.COMPARE.RESULTS$Lm_result      = H1.lm


## read in manuscript results for H1 and H2, so we don't have to re-calculate the randomised p-values
H1.manuscript = read.csv("OUTPUT/TABLES/MANUSCRIPT_T1.csv",  stringsAsFactors = FALSE)
H1.ms         = H1.manuscript[c("Model", "Intercept", "C1", "P1", "Test.stat")]


## input manunscript results for rain into comparison table. Could use "melt", etc?
H1.COMPARE.RESULTS[1, "MS_result"] = H1.ms[1, "Intercept"]
H1.COMPARE.RESULTS[2, "MS_result"] = H1.ms[1, "C1"]
H1.COMPARE.RESULTS[3, "MS_result"] = H1.ms[1, "Test.stat"]
H1.COMPARE.RESULTS[4, "MS_result"] = H1.ms[1, "P1"]

## input manunscript results for rain into comparison table
H1.COMPARE.RESULTS[5, "MS_result"] = H1.ms[2, "Intercept"]
H1.COMPARE.RESULTS[6, "MS_result"] = H1.ms[2, "C1"]
H1.COMPARE.RESULTS[7, "MS_result"] = H1.ms[2, "Test.stat"]
H1.COMPARE.RESULTS[8, "MS_result"] = H1.ms[2, "P1"]

## input phylolm results for temp into comparison table
H1.COMPARE.RESULTS[9,  "MS_result"] = H1.ms[3, "Intercept"]
H1.COMPARE.RESULTS[10, "MS_result"] = H1.ms[3, "C1"]
H1.COMPARE.RESULTS[11, "MS_result"] = H1.ms[3, "Test.stat"]
H1.COMPARE.RESULTS[12, "MS_result"] = H1.ms[3, "P1"]


## create table with rows for regression paramters, and columns for each method
## could incorporate the GAM results too from the main analysis?
model_list_H2_pos   = c("rain position ~ height",  
                        "rain position ~ height",   
                        "rain position ~ height", 
                        "rain position ~ height", 
                        
                        "temp position ~ height",  
                        "temp position ~ height",
                        "temp position ~ height",  
                        "temp position ~ height",
                        
                        "nitrogen position ~ height",  
                        "nitrogen position ~ height",
                        "nitrogen position ~ height",  
                        "nitrogen position ~ height",
                        
                        "rain position ~ log area",  
                        "rain position ~ log area",   
                        "rain position ~ log area", 
                        "rain position ~ log area", 
                        
                        "temp position ~ log area",  
                        "temp position ~ log area",
                        "temp position ~ log area",  
                        "temp position ~ log area",
                        
                        "nitrogen position ~ log area",  
                        "nitrogen position ~ log area",
                        "nitrogen position ~ log area",  
                        "nitrogen position ~ log area")

model_list_H2_width   = c("rain width ~ height",  
                          "rain width ~ height",   
                          "rain width ~ height", 
                          "rain width ~ height", 
                          
                          "temp width ~ height",  
                          "temp width ~ height",
                          "temp width ~ height",  
                          "temp width ~ height",
                          
                          "nitrogen width ~ height",  
                          "nitrogen width ~ height",
                          "nitrogen width ~ height",  
                          "nitrogen width ~ height",
                          
                          "rain width ~ log area",  
                          "rain width ~ log area",   
                          "rain width ~ log area", 
                          "rain width ~ log area", 
                          
                          "temp width ~ log area",  
                          "temp width ~ log area",
                          "temp width ~ log area",  
                          "temp width ~ log area",
                          
                          "nitrogen width ~ log area",  
                          "nitrogen width ~ log area",
                          "nitrogen width ~ log area",  
                          "nitrogen width ~ log area")
 

## build the columns and rows to store the results
table.length                        = length(table.columns)
RESULTS                             = matrix(0, 24, table.length)
colnames(RESULTS)                   = table.columns

## generate table for niche position ~ trait regressions
H2.COMPARE.RESULTS.POS              = as.data.frame(RESULTS)
H2.COMPARE.RESULTS.POS$Phy_N        = length(trees.ss$Genus)
H2.COMPARE.RESULTS.POS$MS_N = length(trees$Genus)
H2.COMPARE.RESULTS.POS$Model        = model_list_H2_pos
H2.COMPARE.RESULTS.POS$Parameter    = parameter_list
H2.COMPARE.RESULTS.POS

## generate table for niche width ~ trait regressions
H2.COMPARE.RESULTS.WIDTH              = as.data.frame(RESULTS)
H2.COMPARE.RESULTS.WIDTH$Phy_N        = length(trees.ss$Genus)
H2.COMPARE.RESULTS.WIDTH$MS_N         = length(trees$Genus)
H2.COMPARE.RESULTS.WIDTH$Model        = model_list_H2_width
H2.COMPARE.RESULTS.WIDTH$Parameter    = parameter_list
H2.COMPARE.RESULTS.WIDTH 


## standard lms for all tree species with measured functional traits: 647 taxa
## output of lapply(vars, comp_lm_position_height, df = trees.ss) so I couldn't get it to work...
rain.pos.height.lm       = comp_lm_position_height(df = trees.ss, colname = "rain")
temp.pos.height.lm       = comp_lm_position_height(df = trees.ss, colname = "temp")
nitrogen.pos.height.lm   = comp_lm_position_height(df = trees.ss, colname = "nitrogen")

rain.width.height.lm     = comp_lm_width_height(df = trees.ss, colname = "rain")
temp.width.height.lm     = comp_lm_width_height(df = trees.ss, colname = "temp")
nitrogen.width.height.lm = comp_lm_width_height(df = trees.ss, colname = "nitrogen")

rain.pos.area.lm         = comp_lm_position_area(df = trees.ss, colname = "rain")
temp.pos.area.lm         = comp_lm_position_area(df = trees.ss, colname = "temp")
nitrogen.pos.area.lm     = comp_lm_position_area(df = trees.ss, colname = "nitrogen")

rain.width.area.lm       = comp_lm_width_area(df = trees.ss, colname = "rain")
temp.width.area.lm       = comp_lm_width_area(df = trees.ss, colname = "temp")
nitrogen.width.area.lm   = comp_lm_width_area(df = trees.ss, colname = "nitrogen")


## phylo lms for all tree species with measured functional traits: 647 taxa
rain.pos.height.phy       = comp_phylolm_position_height(df = trees.ss, colname = "rain", phy = burley_zanne_tree)
temp.pos.height.phy       = comp_phylolm_position_height(df = trees.ss, colname = "temp", phy = burley_zanne_tree)
nitrogen.pos.height.phy   = comp_phylolm_position_height(df = trees.ss, colname = "nitrogen", phy = burley_zanne_tree)

rain.width.height.phy     = comp_phylolm_width_height(df = trees.ss, colname = "rain", phy = burley_zanne_tree)
temp.width.height.phy     = comp_phylolm_width_height(df = trees.ss, colname = "temp", phy = burley_zanne_tree)
nitrogen.width.height.phy = comp_phylolm_width_height(df = trees.ss, colname = "nitrogen", phy = burley_zanne_tree)

rain.pos.area.phy         = comp_phylolm_position_area(df = trees.ss, colname = "rain", phy = burley_zanne_tree)
temp.pos.area.phy         = comp_phylolm_position_area(df = trees.ss, colname = "temp", phy = burley_zanne_tree)
nitrogen.pos.area.phy     = comp_phylolm_position_area(df = trees.ss, colname = "nitrogen", phy = burley_zanne_tree)

rain.width.area.phy       = comp_phylolm_width_area(df = trees.ss, colname = "rain", phy = burley_zanne_tree)
temp.width.area.phy       = comp_phylolm_width_area(df = trees.ss, colname = "temp", phy = burley_zanne_tree)
nitrogen.width.area.phy   = comp_phylolm_width_area(df = trees.ss, colname = "nitrogen", phy = burley_zanne_tree)


## could also get the explanatory power of the phylolm models inside the comp_phylolm functions. EG:
## RSS.phylo  = sum(residuals(fit)^2)
## TSS.phylo  = sum((df$y_var - mean(df$y_var))^2)
## EP.phylo   = 1 - (RSS.phylo/TSS.phylo)


## combine the regressions
H2.pos.lm    = c(rain.pos.height.lm, temp.pos.height.lm, nitrogen.pos.height.lm,
                 rain.pos.area.lm,   temp.pos.area.lm,   nitrogen.pos.area.lm)

H2.width.lm  = c(rain.width.height.lm, temp.width.height.lm, nitrogen.width.height.lm,
                 rain.width.area.lm,   temp.width.area.lm,   nitrogen.width.area.lm)

H2.pos.phy   = c(rain.pos.height.phy, temp.pos.height.phy, nitrogen.pos.height.phy,
                 rain.pos.area.phy,   temp.pos.area.phy,   nitrogen.pos.area.phy)

H2.width.phy = c(rain.width.height.phy, temp.width.height.phy, nitrogen.width.height.phy,
                 rain.width.area.phy,   temp.width.area.phy,   nitrogen.width.area.phy)


## now stitch results in:
H2.COMPARE.RESULTS.POS$Phy_result   = H2.pos.phy
H2.COMPARE.RESULTS.POS$Lm_result    = H2.pos.lm
H2.COMPARE.RESULTS.POS

H2.COMPARE.RESULTS.WIDTH$Phy_result = H2.width.phy
H2.COMPARE.RESULTS.WIDTH$Lm_result  = H2.width.lm
H2.COMPARE.RESULTS.WIDTH


## read in results from main manscript, to extract the random P-values
H2.MS        = read.csv("OUTPUT/TABLES/MANUSCRIPT_T2.csv",  stringsAsFactors = FALSE)


## input manunscript results for niche position into comparison table. 
## Could use "melt", or a loop?
## rain position ~ height results
H2.COMPARE.RESULTS.POS[1, "MS_result"] = H2.MS[1, "Intercept"]
H2.COMPARE.RESULTS.POS[2, "MS_result"] = H2.MS[1, "Coefficient"]
H2.COMPARE.RESULTS.POS[3, "MS_result"] = H2.MS[1, "T.stat"]
H2.COMPARE.RESULTS.POS[4, "MS_result"] = H2.MS[1, "P"]

## temp position ~ height results
H2.COMPARE.RESULTS.POS[5, "MS_result"] = H2.MS[2, "Intercept"]
H2.COMPARE.RESULTS.POS[6, "MS_result"] = H2.MS[2, "Coefficient"]
H2.COMPARE.RESULTS.POS[7, "MS_result"] = H2.MS[2, "T.stat"]
H2.COMPARE.RESULTS.POS[8, "MS_result"] = H2.MS[2, "P"]

## nitrogen position ~ height results
H2.COMPARE.RESULTS.POS[9,  "MS_result"] = H2.MS[3, "Intercept"]
H2.COMPARE.RESULTS.POS[10, "MS_result"] = H2.MS[3, "Coefficient"]
H2.COMPARE.RESULTS.POS[11, "MS_result"] = H2.MS[3, "T.stat"]
H2.COMPARE.RESULTS.POS[12, "MS_result"] = H2.MS[3, "P"]

## rain position ~ area results
H2.COMPARE.RESULTS.POS[13, "MS_result"] = H2.MS[4, "Intercept"]
H2.COMPARE.RESULTS.POS[14, "MS_result"] = H2.MS[4, "Coefficient"]
H2.COMPARE.RESULTS.POS[15, "MS_result"] = H2.MS[4, "T.stat"]
H2.COMPARE.RESULTS.POS[16, "MS_result"] = H2.MS[4, "P"]

## temp position ~ area results
H2.COMPARE.RESULTS.POS[17, "MS_result"] = H2.MS[5, "Intercept"]
H2.COMPARE.RESULTS.POS[18, "MS_result"] = H2.MS[5, "Coefficient"]
H2.COMPARE.RESULTS.POS[19, "MS_result"] = H2.MS[5, "T.stat"]
H2.COMPARE.RESULTS.POS[20, "MS_result"] = H2.MS[5, "P"]

## nitogen position ~ area results
H2.COMPARE.RESULTS.POS[21, "MS_result"] = H2.MS[6, "Intercept"]
H2.COMPARE.RESULTS.POS[22, "MS_result"] = H2.MS[6, "Coefficient"]
H2.COMPARE.RESULTS.POS[23, "MS_result"] = H2.MS[6, "T.stat"]
H2.COMPARE.RESULTS.POS[24, "MS_result"] = H2.MS[6, "P"]
H2.COMPARE.RESULTS.POS

## input manunscript results for niche width into comparison table. 
## Could use "melt", or a loop?
## rain position ~ height results
H2.COMPARE.RESULTS.WIDTH[1, "MS_result"] = H2.MS[7, "Intercept"]
H2.COMPARE.RESULTS.WIDTH[2, "MS_result"] = H2.MS[7, "Coefficient"]
H2.COMPARE.RESULTS.WIDTH[3, "MS_result"] = H2.MS[7, "T.stat"]
H2.COMPARE.RESULTS.WIDTH[4, "MS_result"] = H2.MS[7, "P"]

## temp WIDTHition ~ height results
H2.COMPARE.RESULTS.WIDTH[5, "MS_result"] = H2.MS[8, "Intercept"]
H2.COMPARE.RESULTS.WIDTH[6, "MS_result"] = H2.MS[8, "Coefficient"]
H2.COMPARE.RESULTS.WIDTH[7, "MS_result"] = H2.MS[8, "T.stat"]
H2.COMPARE.RESULTS.WIDTH[8, "MS_result"] = H2.MS[8, "P"]

## nitrogen WIDTHition ~ height results
H2.COMPARE.RESULTS.WIDTH[9,  "MS_result"] = H2.MS[9, "Intercept"]
H2.COMPARE.RESULTS.WIDTH[10, "MS_result"] = H2.MS[9, "Coefficient"]
H2.COMPARE.RESULTS.WIDTH[11, "MS_result"] = H2.MS[9, "T.stat"]
H2.COMPARE.RESULTS.WIDTH[12, "MS_result"] = H2.MS[9, "P"]

## rain WIDTHition ~ area results
H2.COMPARE.RESULTS.WIDTH[13, "MS_result"] = H2.MS[10, "Intercept"]
H2.COMPARE.RESULTS.WIDTH[14, "MS_result"] = H2.MS[10, "Coefficient"]
H2.COMPARE.RESULTS.WIDTH[15, "MS_result"] = H2.MS[10, "T.stat"]
H2.COMPARE.RESULTS.WIDTH[16, "MS_result"] = H2.MS[10, "P"]

## temp WIDTHition ~ area results
H2.COMPARE.RESULTS.WIDTH[17, "MS_result"] = H2.MS[11, "Intercept"]
H2.COMPARE.RESULTS.WIDTH[18, "MS_result"] = H2.MS[11, "Coefficient"]
H2.COMPARE.RESULTS.WIDTH[19, "MS_result"] = H2.MS[11, "T.stat"]
H2.COMPARE.RESULTS.WIDTH[20, "MS_result"] = H2.MS[11, "P"]

## nitogen WIDTHition ~ area results
H2.COMPARE.RESULTS.WIDTH[21, "MS_result"] = H2.MS[6, "Intercept"]
H2.COMPARE.RESULTS.WIDTH[22, "MS_result"] = H2.MS[6, "Coefficient"]
H2.COMPARE.RESULTS.WIDTH[23, "MS_result"] = H2.MS[6, "T.stat"]
H2.COMPARE.RESULTS.WIDTH[24, "MS_result"] = H2.MS[6, "P"]
H2.COMPARE.RESULTS.WIDTH

